# Zendesk Align Status & SLA

## Purpose

A userscript for Zendesk tickets.

Zendesk changed it's styling (2022-02-03). \
The status and SLA buttons used to be vertically centered, now their tops are aligned. \
That annoyed a few people... \
This script restores the vertical alignment of the buttons.

| Old | New |
| - | - |
| ![](img/old.png) | ![](img/new.png) |

## Installation

Load the script in your userscript manager using the direct URL:

https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_align_status_sla/script.user.js

## Changelog

- 1.0
  - Initial release

# Zendesk short SLA

👉 2023-07-31 The views apparently were changed, so this script is no longer needed

## Purpose

A userscript for Zendesk views.

This script changes the `Next SLA breach` title in the table heaer to just `SLA` so the table is a bit more compact.\
The SLA color pills in the column fit nicely in the smaller column.

## Installation

Load the script in your userscript manager using the direct URL:

https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_short_sla/script.user.js

## Warning

Due to how Zendesk updates the tables, this script is not perfect. \
When you switch between Zendesk views you'll see that an incorrect column will have the `SLA` title.
If this bothers you, a page refresh will solve it.

Despite this deficiency I find the script very useful. 

## Changelog

- 1.1
  - Works again after Zendesk layout changes
- 1.0.0
  - Public release

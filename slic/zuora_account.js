'use strict';

function scrapeZuoraAccount(url) {
  let info = {};
  info.url = url;

  const infoRows = Object.fromEntries([...document.querySelectorAll('table#basicInfo_table tr')].map(row =>
    [
      row.querySelector('th h5')?.textContent.split(':')[0].trim(),
      row.querySelector('td')?.textContent.trim()
    ]));
  info.name = infoRows['Name'];
  info.account_number = infoRows['Account Number'];
  info.entity = infoRows['Entity'];

  info.communication_profile = document.querySelector('td span#communicationProfile').textContent;

  [...document.querySelectorAll('div#account_key_contacts > div.z-innerBlock')].forEach( contact => {
    const name = contact.querySelector('span').textContent.trim();
    const email = contact.querySelectorAll('.z-innerBlock')[0].textContent.trim();
    const type = contact.querySelector('span').nextSibling.nextSibling.nextSibling.textContent.trim().slice(1, -1);
    if (type === 'Sold To' || type === 'Bill To / Sold To') {
      info.soldto_name = name;
      info.soldto_email = email;
    }
    if (type === 'Bill To' || type === 'Bill To / Sold To') {
      info.billto_name = name;
      info.billto_email = email;
    }
  });

  // Note to future me:
  // In case scraping subscriptions starts to fail, check if Zuora fixed their 'subsciptions' typo
  info.subscriptions = [];
  [...document.querySelectorAll('table#subsciptions tr')].forEach( (row, index) => {
    if (index === 0) return;
    const cells = row.querySelectorAll('td');
    let subscription = {};
    subscription.number = cells[0].textContent.trim();
    subscription.products = cells[2].textContent.trim();
    subscription.effective = cells[3].textContent.trim();
    subscription.renewal = cells[4].textContent.trim();
    subscription.status = cells[5].textContent.trim();
    info.subscriptions.push(subscription);
  });

  return pasteZuoraAccount(info);
}

function pasteZuoraAccount(info) {
  let text = `Zuora Account information:\n` +
    `URL = ${info.url}\n` +
    `Name = ${info.name}\n` +
    `Account Number = ${info.account_number}\n` +
    `Entity = ${info.entity}\n` +
    `Communication Profile = ${info.communication_profile}\n` +
    `Sold To = ${info.soldto_name} - ${info.soldto_email}\n` +
    `Bill To = ${info.billto_name} - ${info.billto_email}\n`;

  text += 'Subscriptions:\n';
  info.subscriptions.forEach( sub => {
    text += `- Number:    ${sub.number}\n`;
    text += `  Products:  ${sub.products}\n`;
    text += `  Effective: ${sub.effective}\n`;
    text += `  Renewal:   ${sub.renewal}\n`;
    text += `  Status:    ${sub.status}\n`;
  });
  return text;
}

'use strict';

function scrapeCDotLicense(url) {
  let info = {};
  info.url = url;

  let headers = document.querySelectorAll('.card-header');
  let bodies = document.querySelectorAll('.card-body');
  if (headers.length && (headers.length == bodies.length)) {
    for (let i = 0; i < headers.length; i++) {
      // Field name, eg '  First name  \n' -> remove whitespace and LF/CR, lowercase, snake_case -> 'first_name'
      let key = headers[i].textContent.trim().toLowerCase().replace(/ /g, '_');
      let value = headers[i].nextElementSibling.textContent.trim();
      info[key] = value;
    }
  } else {
    toast_error('headers and bodies length not equal');
  }
  return pasteCDotLicense(info);
}

function pasteCDotLicense(info) {
  let text =
    `URL = ${info.url}\n` +
    `Name = ${info.name}\n` +
    `Company = ${info.company}\n` +
    `Email = ${info.email}\n` +
    `Issued at = ${info.issued_at}\n` +
    `Starts at = ${info.starts_at}\n` +
    `Expires at = ${info.expires_at}\n` +
    `Licensed users count = ${info.users_count}\n` +
    `Previous users count = ${info.previous_users_count}\n` +
    `Trueup count = ${info.trueup_count}\n` +
    `Zuora sub = ${info.zuora_subscription}\n` +
    `Zuora name = ${info.zuora_subscription_name}\n` +
    `Trial = ${info.trial}\n` +
    `Type = ${info.type}\n` +
    `Plan = ${info.gitlab_plan}\n` +
    `Notes = ${info.notes}\n` +
    `Creator = ${info.creator}\n`
  ;
  return text;
}

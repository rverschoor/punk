'use strict';

function labelToValue(search_label, labelNodes) {
  // I have to stoopidly loop, as there's no 1:1 relation between labels and values indexes...
  let value = '???';
  for (const node of labelNodes) {
    // outerText gets rid of the helpbutton stuff
    if (node.outerText === search_label) {
      value = node.nextSibling.textContent;
      break;
    }
  }
  return value;
}

function scrapeSfdc(url) {
  let bodyClasses = document.querySelector('body').classList;
  if (bodyClasses.contains('opportunityTab')) {
    return scrapeSfdcOpportunity(url);
  } else if (bodyClasses.contains('accountTab')) {
    return scrapeSfdcAccount(url);
  } else if (bodyClasses.contains('customnotabTab')) {
    return scrapeSfdcSubscription(url);
  }
}

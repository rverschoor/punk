// ==UserScript==
// @name          Zendesk Window Cleaner
// @version       1.2
// @author        Tom McAtee
// @description   Zendesk: minimises the left, right, and bottom composer panes
// @match         https://gitlab.zendesk.com/agent/*
// @license       MIT
// @grant         GM.registerMenuCommand
// @namespace     https://gitlab.com/rverschoor/punk
// @homepageURL   https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_window_cleaner
// @downloadURL   https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_window_cleaner/script.user.js
// @installURL    https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_window_cleaner/script.user.js
// @updateURL     https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_window_cleaner/script.user.js
// @supportURL    https://gitlab.com/rverschoor/punk/issues/new
// ==/UserScript==
'use strict';


(function() {
  'use strict';

  const composerExpand = "[label='Show composer']";
  const composerCollapse = "[label='Hide composer']";

  const leftPaneButtonExpand = "div[data-test-id='column-1'] svg[data-test-id='CollapseButton-styled-chevron-right-icon']";
  const leftPaneButtonCollapse = "div[data-test-id='column-1'] svg[data-test-id='CollapseButton-styled-chevron-left-icon']";

  const rightPaneButtonCollapse = "div[data-test-id='column-3'] svg[data-test-id='CollapseButton-styled-chevron-right-icon']";
  const rightPanelActiveButton = "li > button[aria-pressed='true']";

  let activeButton = null;

  function isRightHandPaneVisible() {
    return (document.querySelector(rightPaneButtonCollapse) !== null);
  }

  function isLeftHandPaneVisible() {
    return (document.querySelector(leftPaneButtonCollapse) !== null);
  }

  function isComposerPaneVisible() {
    return (document.querySelector(composerCollapse) !== null);
  }

  function togglePanes() {
    const composerVisible = isComposerPaneVisible();
    const leftPaneVisible = isLeftHandPaneVisible();
    const rightPaneVisible = isRightHandPaneVisible();
    // If any pane is expanded, collapse all
    if (composerVisible || leftPaneVisible || rightPaneVisible) {
      if (composerVisible) { collapseComposer(); }
      if (leftPaneVisible) { collapseLeftPane(); }
      if (rightPaneVisible) { collapseRightPane(); }
    } else {
      // If all panes are collapsed, expand all
      expandComposer();
      expandLeftPane();
      expandRightPane();
    }
  }

  function collapseComposer() {
    document.querySelector(composerCollapse)?.click();
  }

  function expandComposer() {
    document.querySelector(composerExpand)?.click();
  }

  function collapseLeftPane() {
    document.querySelector(leftPaneButtonCollapse)?.parentElement?.click();
  }

  function expandLeftPane() {
    document.querySelector(leftPaneButtonExpand)?.parentElement?.click();
  }

  function collapseRightPane() {
    rememberRightPanelActiveButton();
    document.querySelector(rightPaneButtonCollapse)?.parentElement?.click();
  }

  function expandRightPane() {
    // If we know which button expanded the pane last time, re-open it
    activeButton?.click();
  }

  function rememberRightPanelActiveButton() {
    activeButton = document.querySelector(rightPanelActiveButton);
  }

  function checkIfPageIsTicket() {
    const currentUrl = window.location.href;
    // Check if the current URL contains the substring 'tickets' to avoid running on non-ticket pages
    if(currentUrl.indexOf("tickets") !== -1 ) {
      togglePanes();
    }
  }

  // Add handling for triggering the script
  GM.registerMenuCommand("Clear View", () => {
    checkIfPageIsTicket();
  });

  function hotKeyHandler(event) {
    if(event.ctrlKey && event.shiftKey && event.code == "KeyA") {
      checkIfPageIsTicket();
    }
  }

  window.addEventListener("keydown", hotKeyHandler, true);
})();
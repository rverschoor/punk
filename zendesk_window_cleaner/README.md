# Zendesk Window Cleaner

## Purpose

A userscript for Zendesk tickets.

This script minimises the left and right hand panes (displaying ZD organisation and requester/application information respectively), in addition to minimising the response composer.

The minimisation can be triggered by:

- Using the Userscript menu in your web browser
- Pressing `shift + ctrl + A` at the same time


## Installation

Load the script in your userscript manager using the direct URL:

https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_window_cleaner/script.user.js

## Configuration

There is no configuration required.

## Caveats

- If the right hand side pane is minimised, it will not reopen if the userscript is triggered again (as the userscript does not record the state of _which_ right hand side tab is open)

## Changelog

- 1.2 Adapted for changed Zendesk screen
- 1.1 Fixed pane consistency and removed debug logs
- 1.0 Public release

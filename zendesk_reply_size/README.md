# Zendesk Reply Size

## Purpose

Use the keyboard to resize the reply editor window.

`Control-Shift-⬆️` key combination to increase the size, and
`Control-Shift-⬇️` to reduce the size.

## Installation

Load the script in your userscript manager using the direct URL:

https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_reply_size/script.user.js

## Changelog

- 1.0 Public release

// ==UserScript==
// @name          Zendesk Reply Size
// @version       1.0
// @author        Rene Verschoor
// @description   Zendesk: resize reply editor
// @match         https://gitlab.zendesk.com/*
// @license       MIT
// @namespace     https://gitlab.com/rverschoor/punk
// @homepageURL   https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_reply_size
// @downloadURL   https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_reply_size/script.user.js
// @installURL    https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_reply_size/script.user.js
// @updateURL     https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_reply_size/script.user.js
// @supportURL    https://gitlab.com/rverschoor/punk/issues/new
// ==/UserScript==

// @@@ TODO
// - DRY code
// - manual drag results in weird behaviour
// - minimize, next step is hide
// - minmax values (0/100 90/10 80/20) seem to change, depending on resolution?
//   i can still manually drag beyond these values

'use strict';

(function () {

  const HOTKEY_UP = [
    {"shift": true, "control": true, "alt": false, "meta": false, "key": "ArrowUp"}
  ]
  const HOTKEY_UPMAX = [
    {"shift": true, "control": true, "alt": true, "meta": false, "key": "ArrowUp"}
  ]
  const HOTKEY_DOWN = [
    {"shift": true, "control": true, "alt": false, "meta": false, "key": "ArrowDown"}
  ]
  const HOTKEY_DOWNMAX = [
    {"shift": true, "control": true, "alt": true, "meta": false, "key": "ArrowDown"}
  ]

  const hotkeyListener = (sequence, callback) => {
    let index = 0;
    let timerId;
    const TIMEOUT = 500;

    return (e) => {
      const step = sequence[index];
      if (e.key === step.key &&
        step.shift === e.shiftKey &&
        step.control === e.ctrlKey &&
        step.alt === e.altKey &&
        step.meta === e.metaKey) {
        if (index === sequence.length - 1) {
          callback();
        }
        clearTimeout(timerId);
        if (index !== sequence.length - 1) {
          timerId = setTimeout(() => index = 0, TIMEOUT);
        }
        index = (index + 1) % sequence.length;
      } else {
        index = 0;
        clearTimeout(timerId);
      }
    };
  };

  const eventListenerUp = hotkeyListener(HOTKEY_UP, () => {
    embiggen();
  });

  const eventListenerUpMax = hotkeyListener(HOTKEY_UPMAX, () => {
    embiggenMax();
  });

  const eventListenerDown = hotkeyListener(HOTKEY_DOWN, () => {
    shrink();
  });

  const eventListenerDownMax = hotkeyListener(HOTKEY_DOWNMAX, () => {
    shrinkMax();
  });

  document.addEventListener('keyup', eventListenerUp, true);
  document.addEventListener('keyup', eventListenerUpMax, true);
  document.addEventListener('keyup', eventListenerDown, true);
  document.addEventListener('keyup', eventListenerDownMax, true);

  function embiggen() {
    const editor = getEditor();
    const values = getValues(editor);
    let val1 = Number(values[2]);
    let val2 = Number(values[4]);
    // console.log(`${val1}-${val2}`);
    val1 = Math.max(val1 - 10, 10);
    val2 = Math.min(val2 + 10, 90);
    // console.log(`${val1}-${val2}`);
    const newValues = `minmax(${values[1]}px, ${val1}fr) minmax(${values[3]}px, ${val2}fr)`;
    setValues(editor, newValues);
  }

  function embiggenMax() {
    const editor = getEditor();
    const values = getValues(editor);
    const newValues = `minmax(${values[1]}px, 10fr) minmax(${values[3]}px, 90fr)`;
    setValues(editor, newValues);
  }

  function shrink() {
    const editor = getEditor();
    const values = getValues(editor);
    let val1 = Number(values[2]);
    let val2 = Number(values[4]);
    // console.log(`${val1}-${val2}`);
    val1 = Math.min(val1 + 10, 80);
    val2 = Math.max(val2 - 10, 20);
    // console.log(`${val1}-${val2}`);
    const newValues = `minmax(${values[1]}px, ${val1}fr) minmax(${values[3]}px, ${val2}fr)`;
    setValues(editor, newValues);
  }

  function shrinkMax() {
    const editor = getEditor();
    const values = getValues(editor);
    const newValues = `minmax(${values[1]}px, 80fr) minmax(${values[3]}px, 20fr)`;
    setValues(editor, newValues);
  }

  function getEditor() {
    return document.querySelector('[data-test-id="convopane-resizable-grid"]');
  }

  // minmax(60px, 70fr) minmax(140px, 30fr) [1]=60 [2]=70 [3]=140 [4]=30
  function getValues(editor) {
    const rows = editor.style.gridTemplateRows;  // E.g. minmax(60px, 60fr) minmax(140px, 40fr)
    const regex = /minmax\((\d+)px, (\d+)fr\) minmax\((\d+)px, (\d+)fr\)/
    const values = rows.match(regex);
    if (values.length !== 5) {
      console.log('Minmax regex returned unexpected number of matches');
      console.log(`Expected 5, got ${values.length}`);
      console.log(rows);
      console.log(values);
    }
    return values;
  }

  function setValues(editor, values) {
    editor.style.gridTemplateRows = values;
  }

})();

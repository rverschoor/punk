// ==UserScript==
// @name          Zendesk Warn Blank Lines
// @version       1.0
// @author        Simon Street
// @description   Warn if there are blank lines at the end of the reply text box
// @match         https://gitlab.zendesk.com/*
// @license       MIT
// @namespace     https://gitlab.com/rverschoor/punk
// @homepageURL   https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_warn_blank_lines
// @downloadURL   https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_warn_blank_lines/script.user.js
// @installURL    https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_warn_blank_lines/script.user.js
// @updateURL     https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_warn_blank_lines/script.user.js
// @supportURL    https://gitlab.com/rverschoor/punk/issues/new
// ==/UserScript==

'use strict';

(function() {

  var regex = /\n\s*$/;
  var re = new RegExp(regex);
  var replyText = false;

  // Watch for changes in the message box
  MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
  const observer = new MutationObserver(function (mutations, observer) {
    checkReply();
  });
  observer.observe(document.body, {childList: true, subtree: true});

  function checkReply() {
    // Find ticket
    const ticket = getActiveTicket();
    if ( !ticket ) { return; }

    // Find public reply
    const replySelector = '[aria-label="Public reply composer"]';
    const replyElement = ticket.querySelector(replySelector);

    // Find submit group
    const submitSelector = '[data-garden-id="buttons.button_group_view"]';
    const submitElement = ticket.querySelector(submitSelector);

    if ( replyElement && replyElement.innerText && replyElement.innerText.length > 1 && re.test(replyElement.innerText) && submitElement ) {
      // If we can see the text, is greater than 1 (an empty box has a line feed in it), matches our regex, set alert
      submitElement.style.boxShadow = '0px 0px 10px 5px red';
    } else if ( submitElement ) {
      // Otherwise clear the warning
      submitElement.style.boxShadow = '';
    }

    // Helper function to find active ticket
    function getActiveTicket() {
      let workspaces = document.querySelectorAll('.workspace');
      let activeWorkspace;
      for (let workspace of workspaces) {
        if (workspace.style['visibility'] !== 'hidden' && workspace.style['display'] !== 'none') {
          activeWorkspace = workspace;
          break;
        }
      }
      return activeWorkspace;
    }
  }
})();

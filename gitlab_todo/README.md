# GitLab - Todo

## Purpose

A userscript for GitLab's Todo list

Use a hotkey to toggle the marking of specific todos.

`Control + t + c` to un/mark `Closed` todos on current page.\
`Control + t + m` to un/mark `Merged` todos on current page.\
`Control + t + a` to un/mark all todos on current page.

## Installation

Load the script in your userscript manager using the direct URL:

https://gitlab.com/rverschoor/punk/-/raw/main/gitlab_todo/script.user.js

## Changelog

- 1.0 Public release

// ==UserScript==
// @name          Textarea Warn
// @version       1.0
// @author        Sam Figueroa
// @description   Remind a user that text areas are prone to loosing large amounts of input and suggest using a dedicated editor.
// @match         *://*/*
// @license       MIT
// @namespace     https://gitlab.com/rverschoor/punk
// @homepageURL   https://gitlab.com/rverschoor/punk/-/tree/main/textarea_warn
// @downloadURL   https://gitlab.com/rverschoor/punk/-/raw/main/textarea_warn/script.user.js
// @installURL    https://gitlab.com/rverschoor/punk/-/raw/main/textarea_warn/script.user.js
// @updateURL     https://gitlab.com/rverschoor/punk/-/raw/main/textarea_warn/script.user.js
// @supportURL    https://gitlab.com/rverschoor/punk/issues/new
// ==/UserScript==

'use strict';

(function() {

  const warningText = 'Consider writing textarea contents in your editor and then it pasting here.'

  function attachWarnings() {
    let warning = document.createElement('p');
    warning.style = "padding: 5px; color: #DD0000; background-color: white";
    warning.appendChild(document.createTextNode(warningText));

    const areas = document.querySelectorAll('textarea');
    areas.forEach( area => {
      area.placeholder = warningText;
      area.after(warning.cloneNode(true));
    })
  }

  attachWarnings();

})();

// ==UserScript==
// @name          Zendesk comment colors
// @version       1.3
// @author        Rene Verschoor
// @description   Zendesk: control comment background colors
// @license       MIT
// @match         https://gitlab.zendesk.com/*
// @require       https://openuserjs.org/src/libs/sizzle/GM_config.js
// @grant         GM.registerMenuCommand
// @namespace     https://gitlab.com/rverschoor/punk
// @homepageURL   https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_comment_colors
// @downloadURL   https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_comment_colors/script.user.js
// @installURL    https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_comment_colors/script.user.js
// @updateURL     https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_comment_colors/script.user.js
// @supportURL    https://gitlab.com/rverschoor/punk/issues/new
// ==/UserScript==

'use strict';

const iframecss = `
  height: 440px;
  width: 350px;
  border: 1px solid;
  border-radius: 3px;
  position: fixed;
  z-index: 9999;
`;

const css = `
  #CommentColorConfig { 
    background: azure; 
  }
  .config_var {
    padding-bottom: 10px;
  }
`;

(function() {

  let gmc = defineConfig();

  let color_enduser = null;
  let color_agent = null;
  let color_internal = null;
  let fullwidth_comment = false;
  let hide_avatar = false;

  // Watch for any change on the Zendesk page
  MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
  const observer = new MutationObserver(function (mutations, observer) {
    po(mutations);
  });
  observer.observe(document.body, { childList: true, subtree: true });

  // Find the comments
  function po(mutations) {
    mutations.forEach( mutation => {
      mutation.target.querySelectorAll('[data-test-id="omni-log-item-message"]').forEach( (comment, index) => {
        go(comment, comment.getAttribute('type'));
      });
    })
  }

  // Change comment background color
  function go(element, type) {
    let color;
    switch (type) {
      case 'end-user':
        color = color_enduser;
        break;
      case 'agent':
        color = color_agent;
        break;
      case 'internal':
        color = color_internal;
    }
    element.style.backgroundColor = color;
    element.style.borderColor = color;
    if (fullwidth_comment) {
      element.style.width = '100%';
      element.parentElement.style.width = '100%';
    }
    if (hide_avatar) {
      hideAvatar();
    }
  }

  function hideAvatar() {
    // Hide avatar column content
    let avatars = document.querySelectorAll('div[data-test-id="omni-log-comment-avatar"]');
    avatars.forEach( avatar => {
      avatar.style.display = 'none'
    })
    // Squash avatar column
    let comments = document.querySelectorAll('article[data-test-id="omni-log-comment-item"]');
    comments.forEach( comment => {
      comment.style.setProperty('--avatar-column-width', '15px')
    })
  }

  GM.registerMenuCommand("Configure", () => {
    configure();
  });

  function configure() {
    gmc.open();
    gmc.frame.style = iframecss;
  }

  function onConfigInit() {
    color_enduser = gmc.get('enduser');
    color_agent = gmc.get('agent');
    color_internal = gmc.get('internal');
    fullwidth_comment = gmc.get('fullwidth');
    hide_avatar = gmc.get('avatar');
  }

  function onSave() {
    location.reload();
  }

  function defineConfig() {
    return new GM_config(
      {
        'id': 'CommentColorConfig',
        'title': 'Comment colors',
        'fields': {
          'enduser':
            {
              'label': 'End user',
              'type': 'text',
              'default': 'rgb(248, 249, 249)'
            },
          'agent':
            {
              'label': 'Agent',
              'type': 'text',
              'default': 'rgb(248, 249, 249)'
            },
          'internal':
            {
              'label': 'Internal',
              'type': 'text',
              'default': 'rgb(255, 247, 237)'
            },
          'fullwidth':
            {
              'label': 'Fullwidth comments',
              'type': 'checkbox',
              'default': false
            },
          'avatar':
            {
              'label': 'Hide avatar column',
              'type': 'checkbox',
              'default': false
            },
        },
        'events': {
          'init': onConfigInit,
          'save': onSave
        },
        'css': css
      });
  }

})();

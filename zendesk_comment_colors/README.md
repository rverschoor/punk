# Zendesk comment colors

## Purpose

A userscript for Zendesk tickets.

This script gives you control over the comment background colors.

You can use it

-if you don't like the new colors introduced by Zendesk end of July 2024
- if you want to adapt the colors to you liking/ability 
- to optimize them for your dark theme plugin

## Installation

Load the script in your userscript manager using the direct URL:

https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_comment_colors/script.user.js

## Configuration

### Colors

Configure the colors through the menu of your userscript manager.\
Colors can be defined as:
- Name, eg `hotpink`, `lightseagreen`
- Hex, eg `#FF69B4`, `#20B2AA`
- RGB, eg `rgb(255, 105, 180)`, `rgb(32, 178, 170)`
- HSL, eg `hsl(330, 100%, 70.59%)`, `hsl(176.71, 69.52%, 41.18%)`

and probably other methods like LAB, LCH, HWB, etc. (didn't test it).

### Fullwidth comments

The background color is applied to the width of the comment.\
If you don't like this irregular coloring, check the `Fullwidth comments` checkbox.

### Hide avatar column

Once you have distinct colors for agent and end user comments, 
the column that shows the avatar is no longer really necessary.\
The name of the commenter is shown above the comment, so you
miss the avatar, but reclaim some horizontal space.

### Defaults

The configuration dialog has an option to restore the default settings.\
Colors will be set to the old-fashioned ones.\
Fullwidth comments is disabled.

## Changelog

- 1.3 Fix fullwidth comments
- 1.2 Hide avatar column
- 1.1 Fullwidth comments
- 1.0 Public release

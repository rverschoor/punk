// ==UserScript==
// @name          Zendesk status colors
// @version       0.0
// @author        Rene Verschoor
// @description   Zendesk: Status colors
// @match         https://gitlab.zendesk.com/*
// @require       https://openuserjs.org/src/libs/sizzle/GM_config.js
// @grant         GM.registerMenuCommand
// @license       MIT
// @namespace     https://gitlab.com/rverschoor/punk
// @homepageURL   https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_status_colors
// @downloadURL   https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_status_colors/script.user.js
// @installURL    https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_status_colors/script.user.js
// @updateURL     https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_status_colors/script.user.js
// @supportURL    https://gitlab.com/rverschoor/punk/issues/new
// ==/UserScript==

'use strict';

const iframecss = `
  height: 600px;
  width: 450px;
  border: 1px solid;
  border-radius: 3px;
  position: fixed;
  z-index: 9999;
`;

const css = `
  #StatusColorConfig { 
    background: azure; 
  }
  .config_var {
    padding-bottom: 10px;
  }
`;

const default_colors = {};
default_colors['new'] = { 'bg': 'rgb(255, 176, 87)', 'txt': 'rgb(112, 56, 21)' };  // hsl(31.79, 100%, 67.06%)
default_colors['open'] = { 'bg': 'rgb(199, 42, 28)', 'txt': 'white' };
default_colors['hold'] = { 'bg': 'rgb(0, 0, 0)', 'txt': 'rgb(47, 57, 65)' };  // actually rgba(0,0,0,0)
default_colors['pending'] = { 'bg': 'rgb(48, 145, 236)', 'txt': 'white' };
default_colors['solved'] = { 'bg': 'rgb(135, 146, 157)', 'txt': 'white' };
default_colors['closed'] = { 'bg': 'rgb(216, 220, 222)', 'txt': 'rgb(73, 84, 92)' };

// https://labex.io/tutorials/javascript-rgb-to-hsl-color-conversion-28603
function RGBToHSL(r, g, b) {
  r /= 255;
  g /= 255;
  b /= 255;
  const l = Math.max(r, g, b);
  const s = l - Math.min(r, g, b);
  const h = s
    ? l === r
      ? (g - b) / s
      : l === g
        ? 2 + (b - r) / s
        : 4 + (r - g) / s
    : 0;
  return [
    60 * h < 0 ? 60 * h + 360 : 60 * h,
    100 * (s ? (l <= 0.5 ? s / (2 * l - s) : s / (2 - (2 * l - s))) : 0),
    (100 * (2 * l - s)) / 2
  ];
}

const default_fontsize = '65';

const letter_status = { 'N': 'new', 'O': 'open', 'H': 'hold', 'P': 'pending', 'S': 'solved', 'C': 'closed' };

(function() {

  let color_option;
  let desaturation;
  let fontsize;
  let muted_colors = {};
  let custom_colors = {
    'new': { 'bg': '', 'txt': '' },
    'open': { 'bg': '', 'txt': '' },
    'hold': { 'bg': '', 'txt': '' },
    'pending': { 'bg': '', 'txt': '' },
    'solved': { 'bg': '', 'txt': '' },
    'closed': { 'bg': '', 'txt': '' }
  };

  let gmc = defineConfig();

  MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
  const observer = new MutationObserver(function (mutations, observer) {
    po(mutations);
  });
  observer.observe(document.body, { childList: true, subtree: true });

  function po(mutations) {
    mutations.forEach( mutation => {
      mutation.target.querySelectorAll('[data-test-id^="status-badge-"]').forEach( (element) => {
        go(element);
      });
    })
  }

  function go(element) {
    switch (color_option) {
      case 'Muted colors':
        setColor(element, muted_colors);
        break;
      case 'Original colors':
        setColor(element, default_colors);
        break;
      case 'Custom colors':
        setColor(element, custom_colors);
        break;
      case 'No colors':
        noColor(element);
        break;
    }
  }

  function setColor(element, colors) {
    // Normally, textContent is the uppercase status letter that's shown.
    // However, if you hover over the ticket to get the summary,
    // it suddenly returns the full lowercase status word  :shrug:
    const letter = element.textContent[0].toUpperCase();
    const status = letter_status[letter];
    element.style.backgroundColor = colors[status]['bg'];
    element.style.color = colors[status]['txt'];
    element.style.fontSize = default_fontsize + '%';
  }

  function noColor(element) {
    element.style.backgroundColor = 'white';
    element.style.color = 'black';
    element.style.fontSize = fontsize + '%';
  }

  function setMutedColors(desaturation) {
    muted_colors['new'] = { 'bg': desaturate(default_colors['new']['bg'], desaturation), 'txt': 'white' };
    muted_colors['open'] = { 'bg': desaturate(default_colors['open']['bg'], desaturation), 'txt': 'white' };
    muted_colors['hold'] = { 'bg': desaturate(default_colors['hold']['bg'], desaturation), 'txt': 'white' };
    muted_colors['pending'] = { 'bg': desaturate(default_colors['pending']['bg'], desaturation), 'txt': 'white' };
    muted_colors['solved'] = { 'bg': desaturate(default_colors['solved']['bg'], desaturation), 'txt': 'white' };
    muted_colors['closed'] = { 'bg': desaturate(default_colors['closed']['bg'], desaturation), 'txt': 'white' };
  }

  function desaturate(rgb, change) {
    const re=/rgb\((\d+),\s*(\d+),\s*(\d+)\)/
    const [fullrgb, r, g, b] = rgb.match(re);
    let [h, s, l] = RGBToHSL(r, g, b);
    return(`hsl(${h}, ${Math.max(Math.min(s - change, 100), 0)}%, ${l}%)`);
  }

  GM.registerMenuCommand("Configure", () => {
    configure();
  });

  function configure() {
    gmc.open();
    gmc.frame.style = iframecss;
  }

  function onConfigInit() {
    color_option = gmc.get('color_option').replace('<br>', '');
    desaturation = gmc.get('desaturation');
    fontsize = gmc.get('fontsize');
    custom_colors['new'] = { 'bg': gmc.get('new_bg'), 'txt': gmc.get('new_txt') };
    custom_colors['open'] = { 'bg': gmc.get('open_bg'), 'txt': gmc.get('open_txt') };
    custom_colors['hold'] = { 'bg': gmc.get('hold_bg'), 'txt': gmc.get('hold_txt') };
    custom_colors['pending'] = { 'bg': gmc.get('pending_bg'), 'txt': gmc.get('pending_txt') };
    custom_colors['solved'] = { 'bg': gmc.get('solved_bg'), 'txt': gmc.get('solved_txt') };
    custom_colors['closed'] = { 'bg': gmc.get('closed_bg'), 'txt': gmc.get('closed_txt') };
    setMutedColors(desaturation);
  }

  function onSave() {
    location.reload();
  }

  function defineConfig() {
    return new GM_config(
      {
        'id': 'StatusColorConfig',
        'title': 'Status configuration',
        'fields': {
          'color_option': {
            'options': ['<br>Original colors', '<br>Muted colors', '<br>Custom colors', '<br>No colors'],
              'label': 'Color option',
              'type': 'radio',
              'default': 'Muted colors'
          },
          'desaturation':
            {
              'label': 'Desaturation for muted colors',
              'type': 'int',
              'default': 20
            },
          'new_bg':
            {
              'label': 'New: background color',
              'type': 'text',
              'default': default_colors['new']['bg']
            },
          'new_txt':
            {
              'label': 'New: text color',
              'type': 'text',
              'default': default_colors['new']['txt']
            },
          'open_bg':
            {
              'label': 'Open: background color',
              'type': 'text',
              'default': default_colors['open']['bg']
            },
          'open_txt':
            {
              'label': 'Open: text color',
              'type': 'text',
              'default': default_colors['open']['txt']
            },
          'hold_bg':
            {
              'label': 'On-hold: background color',
              'type': 'text',
              'default': default_colors['hold']['bg']
            },
          'hold_txt':
            {
              'label': 'On-hold: text color',
              'type': 'text',
              'default': default_colors['hold']['txt']
            },
          'pending_bg':
            {
              'label': 'Pending: background color',
              'type': 'text',
              'default': default_colors['pending']['bg']
            },
          'pending_txt':
            {
              'label': 'Pending: text color',
              'type': 'text',
              'default': default_colors['pending']['txt']
            },
          'solved_bg':
            {
              'label': 'Solved: background color',
              'type': 'text',
              'default': default_colors['solved']['bg']
            },
          'solved_txt':
            {
              'label': 'Solved: text color',
              'type': 'text',
              'default': default_colors['solved']['txt']
            },
          'closed_bg':
            {
              'label': 'Closed: background color',
              'type': 'text',
              'default': default_colors['closed']['bg']
            },
          'closed_txt':
            {
              'label': 'Closed: text color',
              'type': 'text',
              'default': default_colors['closed']['txt']
            },
          'fontsize':
            {
              'label': 'Fontsize (%)',
              'type': 'int',
              'default': default_fontsize
            },
        },
        'events': {
          'init': onConfigInit,
          'save': onSave
        },
        'css': css
      });
  }

})();


# Zendesk status colors

## Purpose

A userscript for Zendesk tickets.

Change the appearance of the ticket status badge.

## Installation

Load the script in your userscript manager using the direct URL:

https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_status_colors/script.user.js

## Configuration

The butt-ugly configuration screen has quite a few options.\
UX is suboptimal, but it's the best I can do at the moment.

### Color option

Choose how you want to present the status. 

- **Original colors**\
  Just use the original colors from Zendesk
- **Muted colors**\
  Desaturize the colors.
- **Custom colors**\
  Define your own colors.
- **No colors**\
  Just show the status letter.

### Muted colors

The original Zendesk color is converted from RGB to HSL, and then its `S` part
(saturation) is reduced with the value you set in the `Desaturation for muted colors` field.

### Custom colors

Define your own background and text color for each status.\
The pre-filled values are the original Zendesk color values.

Colors can be defined any way you specify a CSS color:
- Name, eg `hotpink`, `lightseagreen`
- Hex, eg `#FF69B4`, `#20B2AA`
- RGB, eg `rgb(255, 105, 180)`, `rgb(32, 178, 170)`
- HSL, eg `hsl(330, 100%, 70.59%)`, `hsl(176.71, 69.52%, 41.18%)`

and probably other methods like LAB, LCH, HWB, etc. (didn't test it).

### No colors

No background color is shown, just the status letter.

As the naked letter is quite small, 60% of the standard font size, the `Fontsize`
setting will give you control over its size.

## Changelog

- 1.0 First public release
